﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCHearSphere : MonoBehaviour {

	private NPC npc;

	void Awake () {
		npc = GetComponentInParent<NPC> ();
	}

	void OnTriggerEnter(Collider col) {
		if (col.GetComponent<Character> ()) {
			npc.AddHearedCharacter (col.GetComponent<Character> ());
		}

	}

	void OnTriggerExit(Collider col) {
		if (col.GetComponent<Character> ()) {
			npc.RemoveHearedCharacter (col.GetComponent<Character> ());
		}

	}
}
