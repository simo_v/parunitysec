﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//definiamo le aree VERDE, GIALLA E ROSSA
//se un nemico si trova nell'area VERDE può colpire il personaggio come se quest'ultimo non fosse in copertura
//se un nemico si trova nell'area GIALLA può colpire il personaggio (con una diminuzione di probabilità) solo se quest'ultimo non sta sparando, esponendosi al fuoco
//se un nemico si trova nell'area ROSSA è impossibile che possa colpire il personaggio in copertura, anche se quest'ultimo si sta esponendo

public class Cover : MonoBehaviour {

	public enum Type {low, hiLeft, hiRight, hiBoth, lShaped, uShaped}
	public Type type;

	private List<Character> attackers = new List<Character>();

	public bool isBeingUsed = false;

	private Character coveredChar;


	public Collider coll;

	//reference positions
	public Transform referenceUp;
	public Transform referenceUpLeft;
	public Transform referenceUpRight;
	public Transform referenceDownLeft;
	public Transform referenceDownRight;

	private PlayerController controller;

	//debug
	public GameObject debugSphere;

	void Start () {
		controller = GameObject.FindObjectOfType<PlayerController> ();
		coll = GetComponent<BoxCollider>();

		GetComponentInChildren<Renderer> ().enabled = false;
		coll.enabled = false;
	}
	
	void Update () {
		
		//fai tutto solo se esiste un character in cover
		if (coveredChar != null) {
			
			//controllo ogni character in gioco, quelli che stanno sparando al character in questa copertura li aggiungo alla lista di attackers
			foreach (Character character in controller.GetAllCharacters ()) {
				if (character.GetTargetObject () == coveredChar.gameObject) {
					attackers.Add (character);
				}
			}

			//per ogni attacker nella lista stabilisco la sua posizione e se può colpire il character in questa copertura
			foreach (Character attacker in attackers) {

				//copertura BASSA

				if (type == Type.low) {

					Vector3 dir = attacker.transform.position - referenceUp.position;

					//trovo l'angolo tra il vettore che punta al nemico e la direzione della copertura
					float angle = Vector3.Angle (dir, referenceUp.forward);

					//se l'angolo è minore di 90° si trova nell'area gialla
					if (angle < 90f) {
					
						//area gialla
						attacker.debugCubeRenderer.material.color = Color.yellow;
						attacker.SetArea ("yellow");
					}
					//se l'angolo è maggiore di 90° si trova nell'area verde
					else if (angle > 90f) {
					
						//area verde
						attacker.debugCubeRenderer.material.color = Color.green;
						attacker.SetArea ("green");
					}

				}
					
			//copertura ALTA SINISTRA

			else if (type == Type.hiLeft) {

					Vector3 dir = attacker.transform.position - referenceUpLeft.position;

					//trovo l'angolo tra il vettore che punta al nemico e la direzione della copertura
					float angle = Vector3.Angle (dir, referenceUpLeft.forward);

					//trovo se il nemico si trova a sinistra o destra rispetto alla copertura, usano il vettore dir che punta verso di lui
					float dirNum = AngleDir (referenceUpLeft.forward, dir, Vector3.up);

					//se l'angolo è minore di 90°
					if (angle < 90f) {

						//se il nemico è a sinistra allora si trova nell'area gialla
						if (dirNum == -1) {
							attacker.debugCubeRenderer.material.color = Color.yellow;
							attacker.SetArea ("yellow");
						} 

					//se il nemico è a destra allora si trova nell'area rossa
					else if (dirNum == 1) {
							attacker.debugCubeRenderer.material.color = Color.red;
							attacker.SetArea ("red");
						}
					}

				//se l'angolo è maggiore di 90° si trova nell'area verde
				else if (angle > 90f) {
						//area verde
						attacker.debugCubeRenderer.material.color = Color.green;
						attacker.SetArea ("green");
					}

				} 
			//copertura ALTA DESTRA

			else if (type == Type.hiRight) {
			
					Vector3 dir = attacker.transform.position - referenceUpLeft.position;

					float angle = Vector3.Angle (dir, referenceUpLeft.forward);

					float dirNum = AngleDir (referenceUpLeft.forward, dir, Vector3.up);

					if (angle < 90f) {
						if (dirNum == 1) {
							attacker.debugCubeRenderer.material.color = Color.yellow;
							attacker.SetArea ("yellow");
						} else if (dirNum == -1) {
							attacker.debugCubeRenderer.material.color = Color.red;
							attacker.SetArea ("red");
						}
					} else if (angle > 90f) {
						//area verde
						attacker.debugCubeRenderer.material.color = Color.green;
						attacker.SetArea ("green");
					}

				}

			//copertura ALTA DOPPIA

			else if (type == Type.hiBoth) {
					
					Vector3 dir = attacker.transform.position - referenceUp.position;

					//trovo l'angolo tra il vettore che punta al nemico e la direzione della copertura
					float angle = Vector3.Angle (dir, referenceUp.forward);

					//se l'angolo è minore di 90° si trova nell'area gialla
					if (angle < 90f) {

						//area gialla
						attacker.debugCubeRenderer.material.color = Color.yellow;
						attacker.SetArea ("yellow");
					}
					//se l'angolo è maggiore di 90° si trova nell'area verde
					else if (angle > 90f) {

						//area verde
						attacker.debugCubeRenderer.material.color = Color.green;
						attacker.SetArea ("green");
					}
				} 

			//copertura L

			else if (type == Type.lShaped) {

				} 

			//copertura U

			else if (type == Type.uShaped) {

				}
			}
		}
		
	}

	public void SetCoveredCharacter(Character newChar) {
		coveredChar = newChar;
	}

	public Character GetCoveredCharacter() {
		return coveredChar;
	}

	public void RemoveCoveredCharacter() {
		coveredChar = null;
		isBeingUsed = false;
	}

	//funzinoe che mi permette di determinare se una posizione è situata alla sinistra (-1) o alla destra (1)  di una posizione di riferimento
	float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up) {
		Vector3 perp = Vector3.Cross(fwd, targetDir);
		float dir = Vector3.Dot(perp, up);

		if (dir > 0f) {
			return 1f;
		} else if (dir < 0f) {
			return -1f;
		} else {
			return 0f;
		}
	}

	//TODO gestire le coperture se si overlappano con oggetti

	/*void OnTriggerStay (Collider collider) {
		if (collider.GetComponent<Collider>().tag == "CoverObject") {
			Debug.Log ("collido collido collido");
			gameObject.SetActive (false);
		}
	}

	void OnTriggerEnter (Collider collider) {
		if (collider.GetComponent<Collider>().tag == "CoverObject") {
			gameObject.SetActive (false);
		}
	}*/
}
