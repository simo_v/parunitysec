﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHearSphere : MonoBehaviour {

	public Enemy enemyChar;

	void Awake() {
		enemyChar = GetComponentInParent<Enemy> ();
	}

	void OnTriggerEnter(Collider col) {
		if (col.GetComponent<PlayableCharacter> ()) {
			enemyChar.AddHearedCharacter (col.GetComponent<Character> ());
		}
	}

	void OnTriggerExit(Collider col) {
		if (col.GetComponent<Character> ()) {
			enemyChar.RemoveHearedCharacter (col.GetComponent<Character> ());
		}

	}
}
