﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public LayerMask mask;

	public GameObject npc;

	public int numberOfNpcs = 100;

	private PlayerController controller;

	// Use this for initialization
	void Start () {

		controller = GameObject.FindObjectOfType<PlayerController> ();

		for (int i = 0; i < numberOfNpcs; i++) {
			Instantiate (npc, FindPosition (), Random.rotation);
			if(!controller.GetAllCharacters().Contains(npc.GetComponent<Character>())) {
				controller.GetAllCharacters ().Add (npc.GetComponent<Character> ());
			}
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	Vector3 FindPosition() {

		Vector3 randomPoint;
		UnityEngine.AI.NavMeshHit navHit;
		List<Collider> neighbours = new List<Collider>();

		do {
			randomPoint = new Vector3 (Random.Range (-240, 240), 0, Random.Range (-240, 240));

			UnityEngine.AI.NavMesh.SamplePosition (randomPoint, out navHit, 4.0f, UnityEngine.AI.NavMesh.AllAreas);

			neighbours = new List<Collider>(Physics.OverlapSphere(navHit.position, 10f, mask));

		} while(neighbours.Count > 0 || !UnityEngine.AI.NavMesh.SamplePosition (randomPoint, out navHit, 4.0f, UnityEngine.AI.NavMesh.AllAreas));

		return navHit.position;

	}
}
