﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableCharacter : MonoBehaviour {

	private bool isSelected;

	public string characterName;

	Character character;

	public GameObject selection;

	private PlayerController controller;


	// Use this for initialization
	void Start () {

		controller = GameObject.FindObjectOfType<PlayerController> ();

		character = GetComponent<Character> ();

	}
	
	// Update is called once per frame
	void Update () {

		if(!character.GetState().Equals("dead")) {
			
			if (isSelected == true) {
				selection.SetActive (true);
			}
			else {
				selection.SetActive (false);
			}
		}

		if (character.GetLife () <= 0) {
			controller.namesList.Add (characterName);
		}

	}

	public void SetIsSelected(bool value){
		isSelected = value;
	}

	public bool GetIsSelected() {
		return isSelected;
	}
}
