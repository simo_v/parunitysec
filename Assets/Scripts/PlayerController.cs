﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public static string[] names = {
		"Giangianpaolo",
		"Adalberto",
		"Silvestrino",
		"Bertuccio",
		"Carlo Picchio",
		"Eustachio",
		"Francopìo",
		"Genoveffo",
		"Gioberto",
		"Gesubaldo",
		"Giangilberto",
		"Igidio",
		"Lauro",
		"Luha",
		"Loredano",
		"Liutprando",
		"Matildo",
		"Isabello",
		"Oreste",
		"Pierpaolino",
		"Wando",
		"Pierugo"
	};
	public List<string> namesList = new List<string> (names);

	private bool isFrozen = false;

	public float cameraSpeed = 20;

	private enum GameState { pause, play}
	private GameState gameState;

	private Camera cam;
	private List<PlayableCharacter> playableCharacters = new List<PlayableCharacter> ();
	private List<Character> allCharacters = new List<Character> ();
	private List<Cover> covers = new List<Cover> ();
	private Character character;
	private PlayableCharacter playable;

	private GameObject pause;
	private GameObject freeze;
	private GameObject slomo;

	RaycastHit leftClickHit;
	RaycastHit rightClickHit;

	public LayerMask mask;

	void Start () {
		cam = Camera.main;

		//TODO aggiorna list ogni volta che cambia il numero dei personaggi
		foreach (PlayableCharacter item in GameObject.FindObjectsOfType<PlayableCharacter>()) {
			playableCharacters.Add (item);
		}

		Debug.Log ("Number of playable characters:" + playableCharacters.Count);

		foreach (Character item in GameObject.FindObjectsOfType<Character>()) {
			allCharacters.Add (item);
		}

		Debug.Log ("Number of characters:" + allCharacters.Count);

		foreach (Cover item in GameObject.FindObjectsOfType<Cover>()) {
			covers.Add (item);
		}
			
		pause = GameObject.Find ("Pause");
		pause.SetActive (false);
		gameState = GameState.play;

		freeze = GameObject.Find ("Freeze");
		freeze.SetActive (false);

		slomo = GameObject.Find ("Slomo");
		slomo.SetActive (false);

		foreach (PlayableCharacter item in playableCharacters) {
			int rand = (int)Random.Range (0,namesList.Count);
			item.characterName = namesList [rand];
			namesList.RemoveAt (rand);
		}
	}

	void Update(){

		// SE SONO IN GIOCO
		if (gameState == GameState.play) {

			//
			// LEFT CLICK
			//

			// !! SELECT !!

			//raycast to select character
			if (Input.GetKeyDown (KeyCode.Mouse0)) {
				Ray ray = cam.ScreenPointToRay (Input.mousePosition);
				Physics.Raycast (ray, out leftClickHit, 1000, mask);

				//if the object hit is a character, it should be isSelected = true
				if (leftClickHit.collider.tag == "PlayableCharacter") {
					character = leftClickHit.collider.GetComponent<Character> ();
					playable = leftClickHit.collider.GetComponent<PlayableCharacter> ();
					playable.SetIsSelected (true);

					//if SHIFT is not pressed set isSelected = false for every other Character that is not the one selected
					if (!(Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift))) {
						for (int i = 0; i < playableCharacters.Count; i++) {
							PlayableCharacter playableCharacterLoop = playableCharacters [i].gameObject.GetComponent<PlayableCharacter> ();
							if (playableCharacterLoop != playable) {
								playableCharacterLoop.SetIsSelected (false);
							}
						}
					}
				} else {
					for (int i = 0; i < playableCharacters.Count; i++) {
						PlayableCharacter playableCharacterLoop = playableCharacters [i].gameObject.GetComponent<PlayableCharacter> ();
						playableCharacterLoop.SetIsSelected (false);
					}
				}
			}

			//
			//RIGHT CLICK
			//

			if (Input.GetKeyDown (KeyCode.Mouse1)) {
				Ray ray = cam.ScreenPointToRay (Input.mousePosition);

				if (Physics.Raycast (ray, out rightClickHit, 1000, mask)) {

					for (int i = 0; i < playableCharacters.Count; i++) {
					
						Character characterLoop = playableCharacters [i].gameObject.GetComponent<Character> ();
						PlayableCharacter playableCharacterLoop = playableCharacters [i].gameObject.GetComponent<PlayableCharacter> ();

						//seleziona personaggio
						if (rightClickHit.collider.tag == "NPC") {
						
							if (playableCharacterLoop.GetIsSelected () == true) {

								// !! COMMAND SHOOT, ATTACK & EXECUTE !!
								if (rightClickHit.collider.gameObject != characterLoop.gameObject) {

									//se ha arma ranged
									if (characterLoop.equippedWeapon.GetWeaponType ().Equals ("ranged")) {

										//se il gioco è in freeze
										if (isFrozen) {

											//se il bersaglio è ko EXECUTING
											if (rightClickHit.collider.gameObject.GetComponent<Character> ().GetState ().Equals ("ko")) {
												//aggiungi ordine di esecuzione nella lista
												characterLoop.GiveOrder ("executing", rightClickHit);
											} 
											//se non è ko SHOOTING
											else {
												//aggiungi ordine di sparare nella Order List
												characterLoop.GiveOrder ("shooting", rightClickHit);
											}
										} 
										//se siamo in real time
										else if (!isFrozen) {

											//se il bersaglio è ko cancella tutti gli ordini e aggiungi EXECUTING nella Order List
											if (rightClickHit.collider.gameObject.GetComponent<Character> ().GetState ().Equals ("ko")) {
												characterLoop.CancelAllOrders ();
												characterLoop.GiveOrder ("executing", rightClickHit);
											} 
											//altrimenti cancella tutti gli ordini e aggiungi SHOOTING nella Order List
											else {
												characterLoop.CancelAllOrders ();
												characterLoop.GiveOrder ("shooting", rightClickHit);
											}
										}
									} 
									//se ha arma melee
									else if (characterLoop.equippedWeapon.GetWeaponType ().Equals ("melee")) {

										//se il gioco è in freeze
										if (isFrozen) {

											//se il bersaglio è ko EXECUTING
											if (rightClickHit.collider.gameObject.GetComponent<Character> ().GetState ().Equals ("ko")) {
												//aggiungi ordine di esecuzione nella lista
												characterLoop.GiveOrder ("executing", rightClickHit);
											} 
											//se non è ko SHOOTING
											else {
												//aggiungi ordine di sparare nella Order List
												characterLoop.GiveOrder ("attacking", rightClickHit);
											}
										} 
										//se siamo in real time
										else if (!isFrozen) {

											//se il bersaglio è ko cancella tutti gli ordini e aggiungi EXECUTING nella Order List
											if (rightClickHit.collider.gameObject.GetComponent<Character> ().GetState ().Equals ("ko")) {
												characterLoop.CancelAllOrders ();
												characterLoop.GiveOrder ("executing", rightClickHit);
											} 
											//altrimenti cancella tutti gli ordini e aggiungi ATTACKING nella Order List
											else {
												characterLoop.CancelAllOrders ();
												characterLoop.GiveOrder ("attacking", rightClickHit);
											}
										}

									}
								}
							}
						}
						else if(rightClickHit.collider.tag == "PlayableCharacter") {

							//CANCELLA AZIONI del personaggio selezionato cliccando col destro su di esso
							if (rightClickHit.collider.gameObject == characterLoop.gameObject) {

								characterLoop.GetOrders ().Clear ();
								if (!characterLoop.GetState ().Equals ("closeContact")) {
									characterLoop.SetState ("idle");
								}
							}
						}

					// !! COMMAND MOVE !!
						else if (rightClickHit.collider.tag == "Ground" || (Input.GetButton("ShowCovers") && rightClickHit.collider.tag == "Cover")) {
						
							if (playableCharacterLoop.GetIsSelected () == true) {
								
								//se il gioco è in freeze
								if (isFrozen) {
									
									//aggiungi ordine nella Order List
									characterLoop.GiveOrder("moving",rightClickHit);
								}
								//se siamo in real time
								else if (!isFrozen) {
									
									//cancella tutti gli ordini e aggiungi questo nella Order List
									characterLoop.CancelAllOrders();
									characterLoop.GiveOrder("moving",rightClickHit);
								}
							}
						}
					}
				}
			}

			// !! CAMERA CONTROL !!

			Vector3 moveInput = new Vector3(Input.GetAxisRaw("Horizontal"),Input.GetAxisRaw("UpDown"),Input.GetAxisRaw("Vertical"));
			Vector3 velocity = moveInput.normalized * cameraSpeed;
			cam.transform.position = cam.transform.position + velocity * Time.unscaledDeltaTime;

			//SHOW COVERS
			if (Input.GetButton ("ShowCovers")) {
				foreach (Cover item in covers) {
					item.GetComponentInChildren<SpriteRenderer> ().enabled = true;
					item.coll.enabled = true;

				}
			} else if (Input.GetButtonUp ("ShowCovers")) {
				foreach (Cover item in covers) {
					item.GetComponentInChildren<SpriteRenderer> ().enabled = false;
					item.coll.enabled = false;

				}
			}

			// SLOMOTION
			if (Input.GetKey (KeyCode.V)) {
				Time.timeScale = 0.5f;
				slomo.SetActive (true);
			}

			//FREEZE
			else if (Input.GetButtonDown ("Freeze")) {
				if (!isFrozen) {
					Time.timeScale = 0;
					freeze.SetActive (true);
					isFrozen = true;

				} else if (isFrozen) {
					Time.timeScale = 1;
					freeze.SetActive (false);
					isFrozen = false;
				}
			}

			//PAUSE
			else if (Input.GetButtonDown ("Cancel")) {
				Time.timeScale = 0;
				pause.SetActive (true);
				gameState = GameState.pause;
			}

			//un-slomotion
			else if (Input.GetKeyUp (KeyCode.V)) {
				Time.timeScale = 1;
				slomo.SetActive (false);
			}
		} 

		//SE SONO IN PAUSA
		else if (gameState == GameState.pause) {

			if (Input.GetButtonDown ("Cancel")) {
				Time.timeScale = 1;
				pause.SetActive (false);
				gameState = GameState.play;
			}

		}
	}

	public bool GetIsFrozen() {
		return isFrozen;
	}

	public string GetGameState() {
		return gameState.ToString ();
	}

	public List<PlayableCharacter> GetPlayableCharacters() {
		return playableCharacters;
	}

	public List<Character> GetAllCharacters() {
		return allCharacters;
	}
}
