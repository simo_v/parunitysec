﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Order {

	public string name;

	public RaycastHit hit;

	public Order(string newName, RaycastHit newHit) {
		name = newName;
		hit = newHit;
	}
}
