﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(UnityEngine.AI.NavMeshAgent))]

public class Character: MonoBehaviour {

	//STATS
	//

	public enum State { idle, moving, shooting, ko, executing, dead, closeContact, attacking }
	public State state;

	public enum Area { green, yellow, red, none	}
	public Area area;

	public List<Order> orders = new List<Order> ();

	private bool isBeingExecuted = false;
	private bool isInCover;
	private bool isShooting;
	private bool isMoving;
	private bool isStunned;

	public bool isInTurn;
	public bool isBeingAttacked;
	public bool isStealingTurn = false;
	public bool wantsToAttack = false;

	private bool isRotating = false;

	private bool canExecute = false;
	private bool canShoot = true;
	private bool canRegen = true;
	private bool canTakeDamage = true;

	public float cooldownKO = 20;

	private float stunTimer;
	private float restTimer;
	private int rest;
	private bool isResting = false;

	public int maxLife = 5;
	private int life;

	private float aimTime;

	public int maxEnergy = 100;
	private int energy;
	public float msEnergyRegenRate = 1;
	private float regenTimer;
	private float timeToStartRegenerate;

	private float executionTime; 

	private int defenseShot;

	private PlayableCharacter playable;
	private UnityEngine.AI.NavMeshAgent navMeshAgent;
	public GameObject selection;
	public Transform weaponHold;
	private Material skinMaterial;
	public Color originalSkinColor;
	public UnityEngine.UI.Image myEnergyBar;
	private Color originalEnergyBarColor;

	public ParticleSystem bloodEfx;
	public ParticleSystem deathEfx;
	public CloseContact closeContact;
	private CloseContact currentCC;

	private float flashColor = 0.2f;

	private RaycastHit targetHitLocation;
	private float distance;
	private Character enemy;
	//private Vector3 direction;
	private Cover cover;

	public Weapon equippedWeapon;
	GameObject targetObject =null;

	private Quaternion lookRotation;
	private float timeStartedLerping;

	private PlayerController controller;

	//debug
	public Renderer debugCubeRenderer;

	//temp
	private Vector3 killDirection;

	void Start () {

		controller = GameObject.FindObjectOfType<PlayerController> ();
		navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		EquipWeapon (equippedWeapon);
		msEnergyRegenRate /= 1000;
		energy = maxEnergy;
		regenTimer = msEnergyRegenRate;

		state = State.idle;
		life = maxLife;

		aimTime = equippedWeapon.GetAimTime ();

		skinMaterial = GetComponent<Renderer> ().material;
		originalSkinColor = skinMaterial.color;

		originalEnergyBarColor = myEnergyBar.color;

		restTimer = msEnergyRegenRate;
		rest = maxEnergy;

		area = Area.none;

		executionTime = equippedWeapon.executionTime;
	}
	
	void Update () {

		if (state != State.dead) {

			//se la Order List è > 0 ho degli ordini da eseguire: lo faccio
			//if (orders.Count > 0)
			//	ExecuteOrder ();

			//controlla distanza tra il character e il punto di arrivo
			distance = Vector3.Distance (targetHitLocation.point, gameObject.transform.position);
			if (distance < 1.1f) {
				isMoving = false;
				/*if (state != State.shooting && state != State.ko)
				state = State.idle;*/
			}

			/****************************************** STATI ************************************************************************/

			//
			//STATE CLOSE CONTACT
			//

			if(state== State.closeContact) {

				canRegen = false;

				//interrompo il movimento
				if (isMoving) {
					StopMoving ();
				}
					
				//se sono in turno (aka sono in vantaggio)
				if (isInTurn) {
					//se ho degli ordini
					if (orders.Count > 0) {
						//se tutti i personaggi nel close contact che puntano a me sono in stun, eseguo l'ordine (scappo)
						if (currentCC.AllAttackersInStun (this) == true) {
							currentCC.RemoveCharacter (this);
							ExecuteOrder ();
							isInTurn = false;
							isBeingAttacked = false;
							isStealingTurn = false;
						} 
						//altrimenti se volevo scappare ma c'è gente che mi attacca salto il turno
						else {
							currentCC.SkipTurn (this);
						}
					} 
					//se non ha ordini e nessuno sta tentando di rubargli il turno, attacca
					else if (orders.Count == 0 && currentCC.NooneIsStealingTurn (this) == true) {
						if (energy > 0) {
							RotateTowards (targetObject.transform.position);
							equippedWeapon.Attack ();
						} else {
							currentCC.SkipTurn (this);
						}
					} 
					//se qualcuno sta cercando di rubare il turno
					else if (orders.Count == 0 && currentCC.NooneIsStealingTurn (this) == false) {
						defenseShot = Random.Range (90, 100) - equippedWeapon.meleeDefense;
					}
				}
				//altrimenti se sto subendo (aka è in svantaggio)
				else if (isBeingAttacked) {
					//se ero in idle (senza ordini) allora rispondo all'attacco (imposto come mio target chi mi attacca), 
					//altrimenti dovrebbe tornare a fare quello che stava facendo (lo shooting lo reimposto nello stato attacking, il resto vien da se)
					if (orders.Count == 0) {
						if (currentCC.GetAttacker () != null) {
							targetObject = currentCC.GetAttacker ().gameObject;
						}
					}
					defenseShot = Random.Range (90, 100) - equippedWeapon.meleeDefense;
				}

				// altrimenti se non è nessuno dei due
				else {
					Character targetCharacter = targetObject.GetComponent<Character> ();
					//se non sono stunnato
					if (!isStunned) {
						//se il mio target sta attaccando
						if (targetCharacter.isInTurn) {
							//se nessun altro sta rubando il turno
							if (closeContact.NooneIsStealingTurn (targetObject.GetComponent<Character> ()) == true) {
								//provo a rubare il turno
								isStealingTurn = true;
							}
						}
						//se il mio target sta subendo
							//aspetto che passi in attacco e poi posso attaccare

						//se non sta nè attaccando nè subendo dovrei creare un altro close combat per questi due
						else if (!targetCharacter.isInTurn && !targetCharacter.isBeingAttacked) {
							currentCC.RemoveCharacter (targetCharacter);
							currentCC.RemoveCharacter (this);

							currentCC = Instantiate (closeContact, targetObject.transform.position, Quaternion.identity);
							//targetCharacter.SetTargetObject (gameObject);
							targetCharacter.isBeingAttacked = true;
							currentCC.Insert (targetCharacter);
							currentCC.Insert (GetComponent<Character> ());
							isStealingTurn = false;
							isInTurn = true;
						}
					}
					//se sono stunnato isStealing turn = false
					else {
						isStealingTurn = false;
					}

					//se orders.count > 0 eseguo l'ordine
					if (orders.Count > 0) {
						ExecuteOrder ();
					}
				}

			}
			//
			//STATE ATTACK
			//

			if (state == State.attacking) {

				wantsToAttack = true;

				if(!isStunned && rest == maxEnergy) {
				//cambio colore
					if (skinMaterial.color != originalSkinColor) {
						skinMaterial.color = Color.Lerp (skinMaterial.color, originalSkinColor, 5 * Time.deltaTime);
					}

					if (isInCover) {
						isInCover = false;
						cover.RemoveCoveredCharacter ();
					}

					if (currentCC != null) {
						currentCC.RemoveCharacter (this);
					}

					//se la Order List è > 0 ho degli ordini da eseguire: lo faccio
					if (orders.Count > 0) {
						ExecuteOrder ();
					}

					//TODO se io sono già in close contact (nel caso in cui si cambi bersaglio forse serve)

					//mi muovo verso il nemico
					navMeshAgent.SetDestination (targetObject.transform.position);
					isMoving = true;

					//se sono arrivato mi fermo
					if (Vector3.Distance (transform.position, targetObject.transform.position) <= 3f) {
						StopMoving ();

						Character targetCharacter = targetObject.GetComponent<Character> ();

						if (targetCharacter != null) {
							//se non è già in close contact
							if (!targetCharacter.GetState ().Equals ("closeContact")) {
								//istanzia close contact tra me e il nemico
								currentCC = Instantiate (closeContact, targetObject.transform.position, Quaternion.identity);

								//se stava sparando a qualcuno, gli ridò l'odine prima di metterlo nello stato closeContact, almeno appena parerà il colpo tornerà a fare quello che stava facendo
								if (targetCharacter.GetState ().Equals ("shooting")) {
									Ray ray = new Ray (Vector3.up, targetCharacter.GetTargetObject ().transform.position);
									RaycastHit hit;
									Physics.Raycast (ray, out hit);
									targetCharacter.GiveOrder ("shooting", hit);
								}
								isInTurn = true;
								//targetCharacter.SetTargetObject (gameObject);
								targetCharacter.isBeingAttacked = true;
								currentCC.Insert (targetCharacter);
								currentCC.Insert (this);
							
								orders.Clear ();
							}
							//altrimenti se è già in close contact, dipende da cosa sta facendo
							else {
								//se è attaccato o sta attaccando
								if (targetCharacter.isBeingAttacked || targetCharacter.isInTurn) {
									//inserisciti nel close contact ma senza essere in turno o senza essere attaccato
									targetCharacter.GetCurrentCC ().Insert (this);
									isInTurn = false;
									isBeingAttacked = false;
								}
								//se non sta attaccando o non è attaccato
								else {
									isInTurn = true;
									//toglilo dal suo close contact e istanziane un'altro
									targetCharacter.GetCurrentCC ().RemoveCharacter (targetCharacter);

									currentCC = Instantiate (closeContact, targetObject.transform.position, Quaternion.identity);
									targetCharacter.SetTargetObject (gameObject);
									targetCharacter.isBeingAttacked = true;
									currentCC.Insert (targetCharacter);
									currentCC.Insert (this);
								}
								orders.Clear ();
							}
						} else
							state = State.idle;
					}
					//se invece la distanza è minore lerpo alla distanza giusta
					else {

					}
				}
			}

			//
			//STATE EXECUTING
			//

			if (state == State.executing) {

				wantsToAttack = false;

				if (!isStunned) {
					if (skinMaterial.color != originalSkinColor && !canExecute)
						skinMaterial.color = Color.Lerp (skinMaterial.color, originalSkinColor, 5 * Time.deltaTime);

					//se la Order List è > 0 ho degli ordini da eseguire: lo faccio
					if (orders.Count > 0) {
						ExecuteOrder ();
					}
				
					if (currentCC != null) {
						currentCC.RemoveCharacter (this);
					}
					//se il nemico è ancora "ko" e nessuno lo sta ancora eseguendo procedo
					if (targetObject.gameObject.GetComponent<Character> ().GetState ().Equals ("ko") && targetObject.gameObject.GetComponent<Character> ().GetIsBeingExecuted () == false) {

						if (isInCover) {
							isInCover = false;
							cover.RemoveCoveredCharacter ();
						}

						//mi muovo verso il nemico (dovrò settare una posizione specifica per gestire l'eventuale animazione)
						navMeshAgent.SetDestination (targetObject.transform.position);
						isMoving = true;

						//se sono arrivato mi fermo
						if (Vector3.Distance (transform.position, targetObject.transform.position) <= 2.25f) {
							StopMoving ();
							canExecute = true;
						}
					}
				//altrimenti vado in idle
				else if (!canExecute) {
						orders.Clear ();
						state = State.idle;
						executionTime = equippedWeapon.executionTime;
					}

					if (canExecute) {
						//comunico al nemico che lo sto eseguendo
						targetObject.GetComponent<Character> ().SetIsBeingExecuted (true);

						//mi giro verso il nemico e preparo l'attacco
						RotateTowards (targetObject.transform.position);

						executionTime -= Time.deltaTime;

						FlashColor (originalSkinColor);

						// se vengo colpito interrompo l'esecuzione

						//se sono pronto attacco
						if (executionTime <= 0) {

							if (skinMaterial.color != originalSkinColor)
								skinMaterial.color = originalSkinColor;
						
							orders.Clear ();

							if (equippedWeapon.GetWeaponType ().Equals ("ranged")) {
								equippedWeapon.Shoot ();
							}

							else if (equippedWeapon.GetWeaponType ().Equals ("melee")) {
								equippedWeapon.Attack ();
							}

							executionTime = equippedWeapon.executionTime;

							state = State.idle;
						}
					}
				}
			}

			//
			//STATE SHOOTING
			//

			if (state == State.shooting) {

				wantsToAttack = false;

				//cambio colore mesh
				if (!isInCover && skinMaterial.color != originalSkinColor) {
					skinMaterial.color = Color.Lerp (skinMaterial.color, originalSkinColor, 5 * Time.deltaTime);
				} else if (isInCover && skinMaterial.color != Color.cyan) {
					skinMaterial.color = Color.Lerp (skinMaterial.color, Color.cyan, 5 * Time.deltaTime);
				}

				if (!isStunned) {

					//se la Order List è > 0 ho degli ordini da eseguire: lo faccio
					if (orders.Count > 0) {
						ExecuteOrder ();
					}

					canExecute = false;

					//per armi come il fucile a pompa potrebbe essere necessario avere un array di nemici, per ora fottesega
					//prendo un riferimento al component character del targetObject
					enemy = targetObject.GetComponent<Character> ();

					if (enemy.GetIsInCover () == false) {
						area = Area.none;
					}

					//interrompo il movimento
					if (isMoving) {
						StopMoving ();
					}

					if (currentCC != null) {
						currentCC.RemoveCharacter (this);
					}

					//sparo solo se posso farlo, e se l'avversario ha canTakeDamage = true
					if (canShoot && enemy.GetCanTakeDamage () == true) {
						aimTime -= Time.deltaTime;
						isShooting = true;

						//TODO vedere se metterlo nell'update diretto o farlo girare solo se può colpire
						//procedi a girare il personaggio
						//SetRotationDirection (targetObject.transform.position);
						RotateTowards (targetObject.transform.position);
					}

					//altrimenti se non posso sparare
					else if (!canShoot) {
						isShooting = false;
						aimTime = equippedWeapon.GetAimTime ();

						//TODO vedere se farlo o meno, i nemici continueranno a colpirlo
						//se è in cover tornaci
						//if(isInCover) 
						//transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation(-cover.transform.forward), 7 * Time.deltaTime);
				
						//vediamo perchè
						//se l'energia è al minimo significa che non posso sparare per quello, aspetta che si riposi il character e poi setta canShoot = true
						if (energy == 0) {
							//Debug.Log ("non ho energia per sparare");

						}
						if (rest == maxEnergy) {
							canShoot = true;
						}
					}
					// se il nemico non può essere colpito è perchè è KO, vai in idle
					else if (enemy.GetCanTakeDamage () == false) {
				
						isShooting = false;
						aimTime = equippedWeapon.GetAimTime ();

						//Debug.Log ("non posso colpirlo");
						if (orders.Count > 0) {
							orders.RemoveAt (0);
						}
						state = State.idle;
					}

					//se il tempo di mira è finito
					if (aimTime < 0) {
						aimTime = 0;
						//se lo stato è shooting e isShooting è vero
						if (state == State.shooting && isShooting) {
							equippedWeapon.Shoot ();

							//sto già eseguendo l'ordine, lo rimuovo dalla Order List
							if (orders.Count > 0)
								orders.RemoveAt (0);
						}
				
					}
				}
			}

			//
			//STATE IDLE
			//

			if (state == State.idle) {

				StopMoving ();

				area = Area.none;

				//per ora lo lascio stare, servirebbe a riprendere ad attaccare un personaggio anche dopo che mi ha parato il colpo ed è uscito dal CC
				/*if (wantsToAttack && isStunned) {
					Ray ray = new Ray (Vector3.up, targetObject.transform.position);
					RaycastHit hit;
					Physics.Raycast (ray, out hit);
					GiveOrder ("attacking", hit);

					wantsToAttack = false;
				} else
					wantsToAttack = false;*/

				// Fade the character color to match the original color
				if (!isInCover && skinMaterial.color != originalSkinColor) {
					skinMaterial.color = Color.Lerp (skinMaterial.color, originalSkinColor, 5 * Time.deltaTime);
				}
				else if (isInCover && skinMaterial.color != Color.cyan) {
					skinMaterial.color = Color.Lerp (skinMaterial.color, Color.cyan, 5 * Time.deltaTime);
				}

				aimTime = equippedWeapon.GetAimTime ();

				canTakeDamage = true;
				canExecute = false;
				isShooting = false;
				isMoving = false;

				//se sono in cover ci sarebbe da fare l'animazione, per ora ruoto il character
				if (isInCover) {
					//RotateTowards (-cover.transform.forward);
					transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (-cover.transform.forward), 7 * Time.deltaTime);
				}

				if (!isStunned) {
					//se sono in idle e la Order List non è vuota, ho ricevuto ordini: li eseguo
					if (orders.Count > 0) {
						ExecuteOrder ();
					}
				}
			}

			//
			//STATE MOVING
			//

			if (state == State.moving) {

				wantsToAttack = false;

				//cambio colore mesh se sono arrivato qui dallo stato ko
				if (!isInCover && skinMaterial.color != originalSkinColor) {
					skinMaterial.color = Color.Lerp (skinMaterial.color, originalSkinColor, 5 * Time.deltaTime);
				}

				if (!isStunned) {

					//se la Order List è > 0 ho degli ordini da eseguire: lo faccio
					if (orders.Count > 0) {
						ExecuteOrder ();
					}

					area = Area.none;

					//resetta l'aim time
					aimTime = equippedWeapon.GetAimTime ();

					canExecute = false;
					isShooting = false;
					isMoving = true;
					if (isInCover) {
						isInCover = false;
						cover.RemoveCoveredCharacter ();
					}

					if (currentCC != null) {
						currentCC.RemoveCharacter (this);
					}

					//se la destinazione è una cover
					if (targetHitLocation.collider.tag == "Cover") {

						//nell'execute order ho già settato questa posizione come la posizione del transform della cover, almeno il calcolo della distance funziona bene
						navMeshAgent.SetDestination (targetHitLocation.point);

						//se la cover è già usata da un altro mi ci avvicino, mi fermo prima e non mi metto in cover
						if (cover != null && cover.isBeingUsed && cover.GetCoveredCharacter () != gameObject.GetComponent<Character> ()) {

							//se sono arrivato a destinazione
							if (!navMeshAgent.pathPending) {
								if (distance < 3) {

									StopMoving ();
									orders.RemoveAt (0);

									//se c'è un ordine nella List allora passa a quello, altrimenti vai in idle
									if (orders.Count > 0) {
										ExecuteOrder ();
									} else
										state = State.idle;
								}
							}
						} 
					//invece se non è usata mi ci metto io
					else {
							//se sono arrivato a destinazione
							if (!navMeshAgent.pathPending) {
								if (distance < 1.0) {
								
									orders.RemoveAt (0);

									cover.isBeingUsed = true;
									isInCover = true;
									cover.SetCoveredCharacter (GetComponent<Character> ());

									skinMaterial.color = Color.cyan;



									//se c'è un ordine nella List allora passa a quello, altrimenti vai in idle
									if (orders.Count > 0) {
										ExecuteOrder ();
									} else
										state = State.idle;
								}
							}
						}

					} 
					//se la destinazione è un punto sul ground
					else if (targetHitLocation.collider.tag == "Ground") {

						navMeshAgent.SetDestination (targetHitLocation.point);

						if (isInCover) {
							isInCover = false;
							cover.RemoveCoveredCharacter ();

							skinMaterial.color = originalSkinColor;
						}

						//se sono arrivato a destinazione
						if (!navMeshAgent.pathPending) {
							if (distance < 1.5) {
					
								orders.RemoveAt (0);

								//se c'è un ordine nella List allora passa a quello, altrimenti vai in idle
								if (orders.Count > 0) {
									ExecuteOrder ();
								} else
									state = State.idle;
							}
						}
					}
				}
			}

			//
			//STATE KO
			//

			if (state == State.ko) {

				wantsToAttack = false;

				canTakeDamage = false;

				if (life <= 0) {
					controller.GetPlayableCharacters ().Remove (GetComponent<PlayableCharacter> ());
					Instantiate (deathEfx.gameObject, gameObject.transform.position, Quaternion.FromToRotation (Vector3.forward, killDirection));
					Renderer[] renderers = GetComponentsInChildren<Renderer> ();
					foreach (Renderer item in renderers) {
						item.enabled = false;
					}
					state = State.dead;
					Destroy (gameObject, 5 * Time.deltaTime);
				}

				if (isInCover && cover != null) {

					isInCover = false;
					cover.RemoveCoveredCharacter ();
				} 

				if (currentCC != null) {
					currentCC.RemoveCharacter (this);
				}

				if (skinMaterial.color != Color.red) {
					//animazione ko, per ora cambio il colore della mesh in rosso
					skinMaterial.color = Color.Lerp (skinMaterial.color, Color.red, 2 * Time.deltaTime);
				}

				if (isMoving) {
					StopMoving ();
				}

				area = Area.none;

				canExecute = false;
				isResting = false;
				rest = 100;

				aimTime = equippedWeapon.GetAimTime ();

				canRegen = false;
				canTakeDamage = false;

				//se non sta per essere eseguito fai partire il cooldown prima di riprendersi
				if (!isBeingExecuted)
					BeginCooldownKO ();
				//altrimenti se sta per essere eseguito non curarti del cooldown e fai un defenseShot
				else {
					defenseShot = Random.Range (30, 100);
				}
			}


			/************************************************************************************************************************/

			if (state != State.ko) {
				if (isInCover) {
					StopMoving ();
						//se la posizione non è quella della cover.. da fare
						Vector3.Lerp (transform.position, cover.transform.position, 1 * Time.deltaTime);

					if(skinMaterial.color != Color.cyan)
						skinMaterial.color = Color.Lerp (skinMaterial.color, Color.cyan, 5 * Time.deltaTime);
				}
				else if (skinMaterial.color != originalSkinColor)
					skinMaterial.color = Color.Lerp (skinMaterial.color, originalSkinColor , 5 * Time.deltaTime);
			}

			if (area == Area.none)
				debugCubeRenderer.material.color = Color.white;

			//se energia è minore di 0, allora è = 0, non può sparare e deve far partire il timer di riposo con isResting
			if (energy <= 0) {
				energy = 0;
				rest = energy;
				canShoot = false;
				isResting = true;
			}
		
			//se isShooting non si muove e non rigenera
			if (isShooting) {
				canRegen = false;
				isMoving = false;
			}

			//se ne ha la possibilità rigenera l'energia
			if (canRegen) {
				RegenerateEnergy ();
			} else { //altrimenti aspetta il momento giusto
				timeToStartRegenerate -= Time.deltaTime;
				if (timeToStartRegenerate < 0) {
					canRegen = true;
					myEnergyBar.color = originalEnergyBarColor;
				}
			}

			//se isResting è true fai partire il timer di riposo
			if (isResting) {
				Rest ();
			}

			if (rest > maxEnergy) {
				rest = maxEnergy;
				isResting = false;
			}

			if (isStunned) {
				UnStun ();
			}

		}
	}

	void FixedUpdate() {

		//provo a ruotare nel fixed update, se funziona passerò tutte le rotazioni e lerp qui
		if (isRotating) {
			float timeSinceStarted = Time.time - timeStartedLerping;
			float percentageComplete = timeSinceStarted/0.1f;

			transform.rotation = Quaternion.Slerp (transform.rotation, lookRotation, percentageComplete);

			if (percentageComplete >= 1f) {
				isRotating = false;
			}
		}
	}
		

	/************************************FUNCTIONS*****************************************************************/

	void OnCollisionEnter(Collision collision){
		//se collide con un altro character ed è molto vicino a destinazione fermati, c'è già qualcuno li
		if ((collision.collider.gameObject.tag != "Character") && (distance < 2f)) {
			StopMoving ();
		}
	}

	public void EquipWeapon(Weapon weaponToEquip) {
			
		equippedWeapon = Instantiate (weaponToEquip, weaponHold.position, weaponHold.rotation) as Weapon;
		equippedWeapon.transform.parent = weaponHold;
	}


	void StopMoving() {
		navMeshAgent.Stop();
		if (navMeshAgent.hasPath) {
			navMeshAgent.ResetPath ();
		}
		navMeshAgent.Resume ();
		isMoving = false;
	}
		
	public int GetEnergy() {
		return energy;
	}

	public void SetEnergy(int value) {
		energy = value;
	}

	public int GetMaxEnergy() {
		return maxEnergy;
	}

	public int GetRest() {
		return rest;
	}

	void RegenerateEnergy() {
		if(energy < maxEnergy){
			regenTimer -= Time.deltaTime;
			if (regenTimer < 0) {
				energy++;
				regenTimer = msEnergyRegenRate;
			}
		}
	}

	void Rest() {
		restTimer -= Time.deltaTime;
		if (restTimer < 0) {
			rest++;
			restTimer = msEnergyRegenRate;
		}
	}

	void UnStun() {
		stunTimer -= Time.deltaTime;
		if (stunTimer < 0) {
			isStunned = false;
		}
	}

	public void Stun(float amount) {
		isStunned = true;
		stunTimer = amount;
	}

	public void SetStun(bool value) {
		isStunned = value;
	}

	public bool GetIsStunned() {
		return isStunned;
	}

	public bool GetIsMoving() {
		return isMoving;
	}

	public GameObject GetTargetObject(){
		return targetObject;
	}

	public void SetTargetObject(GameObject value){
		targetObject = value;
	}

	public void SetTimeToStartRegenerate(float time) {
		timeToStartRegenerate = time;
	}

	public void SetCanShoot(bool value) {
		canShoot = value;
	}

	public void SetCanRegen(bool value) {
		canRegen = value;
	}

	public void SetLife(int value) {
		life = value;
	}

	public int GetLife() {
		return life;
	}
		
	public void SetCanTakeDamage(bool value) {
		canTakeDamage = value;
	}

	public bool GetCanTakeDamage() {
		return canTakeDamage;
	}

	public void SetState(string value) {
		state = (State)System.Enum.Parse(typeof(State), value);
	}

	public string GetState() {
		return state.ToString ();
	}

	void BeginCooldownKO() {
		cooldownKO -= Time.deltaTime;
		if (cooldownKO < 0) {
			state = State.idle;
			cooldownKO = 10;
			energy = maxEnergy;
		}
	}

	public void GiveOrder(string name, RaycastHit hit) {
		
		//se ci sono ordini nella List, aggiungi questo solo se l'ultimo NON è shooting o attacking o executing (chiudono la pila di ordini)
		if (orders.Count > 0 && orders.Count < 8) {
			if (!orders [orders.Count - 1].name.Equals ("shooting") && !orders [orders.Count - 1].name.Equals ("executing") && !orders [orders.Count - 1].name.Equals ("attacking")) {
				orders.Add (new Order (name, hit));
			}

		//invece se non ci sono ordini nella List, aggiungi tranquillamente questo
		} else if (orders.Count == 0) {
			orders.Add (new Order (name, hit));
		}
	}

	public void CancelAllOrders() {
		orders.Clear ();
	}

	void ExecuteOrder(){
		
		//eseguo l'ordine solo se non sono ko
		if (state != State.ko) {

			//se l'ordine è shooting o executing o attacking utilizzo come target ciò che ha colpito il raycast
			if(orders[0].name.Equals("shooting") || orders[0].name.Equals("executing") || orders[0].name.Equals("attacking"))
				targetObject = orders [0].hit.collider.gameObject;

			//se l'ordine è moving utilizzo come posizione da raggiungere il punto in cui ha colpito il raycast
			if (orders [0].name.Equals ("moving")) {

				//se ero in cover la resetto
				if (isInCover) {
					isInCover = false;
					cover.RemoveCoveredCharacter ();
				}

				//se è stato cliccato il terreno
				if (orders [0].hit.collider.tag == "Ground")
					targetHitLocation = orders [0].hit;
							
				//se invece è stata cliccata una cover
				else if (orders [0].hit.collider.tag == "Cover") {
					targetHitLocation = orders [0].hit;
					targetHitLocation.point = targetHitLocation.collider.gameObject.transform.position;
					cover = targetHitLocation.collider.gameObject.GetComponent<Cover> ();

				}
			}
			//passo allo State comunicato nella variabile Name dell'ordine
			state = (State)System.Enum.Parse (typeof(State), orders [0].name);
		}
	}

	public List<Order> GetOrders(){
		return orders;
	}

	public void SetEnergyBarColor(Color color) {
		myEnergyBar.color = color;
	}

	public void DamageEfx(Vector3 directionEFX) {
		Destroy(Instantiate (bloodEfx.gameObject, transform.position, Quaternion.FromToRotation(Vector3.forward, directionEFX)) as GameObject, 2f);
	}

	public bool GetIsInCover() {
		return isInCover;
	}

	public void SetIsInCover(bool value) {
		isInCover = value;
	}

	public void SetArea(string value) {
		area = (Area)System.Enum.Parse(typeof(Area), value);
	}

	public string GetArea() {
		return area.ToString ();
	} 

	public Cover GetCover() {
		return cover;
	}

	public void SetKillDirection(Vector3 dir) {
		killDirection = dir;
	}

	public void SetIsBeingExecuted(bool value){
		isBeingExecuted = value;
	}

	public bool GetIsBeingExecuted(){
		return isBeingExecuted;
	}

	private void FlashColor(Color color) {

		if (executionTime > 1) {
			flashColor -= Time.deltaTime;
			if (flashColor < 0) {
				if (skinMaterial.color != Color.white) {
					skinMaterial.color = Color.white;
					flashColor = 0.3f;
				}
				else if (skinMaterial.color == Color.white) {
					skinMaterial.color = color;
					flashColor = 0.3f;
				}
			}
		}
		else if (executionTime <= 1) {
			flashColor -= Time.deltaTime;
			if (flashColor < 0) {
				if (skinMaterial.color != Color.white) {
					skinMaterial.color = Color.white;
					flashColor = 0.05f;
				}
				else if (skinMaterial.color == Color.white) {
					skinMaterial.color = color;
					flashColor = 0.05f;
				}
			}
		}
	}

	public int GetDefenseShot() {
		return defenseShot;
	}

	public void SetSkinColor(Color color) {
		skinMaterial.color = color;
	}

	public void TakeEnergyDamage(int damage, Character attacker) {
		if (state == State.closeContact) {
			RotateTowards (attacker.transform.position);
		}
		energy -= damage;
		canRegen = false;
		timeToStartRegenerate = 5;
		myEnergyBar.color = Color.red;
		skinMaterial.color = Color.red;
		DamageEfx (attacker.transform.forward);
	}

	public void TakeLifeDamage(Character attacker) {
		life -= 1;
		killDirection = transform.position - attacker.transform.position;

		RotateTowards (attacker.transform.position);

		if (isInCover) {

			//sbalzalo dalla cover
			GetComponent<Rigidbody> ().AddForce (-GetCover ().transform.forward.normalized * 1000, ForceMode.Impulse);

		} 
		else {
			//sbalzalo indietro
			Vector3 fallingForce = transform.position - attacker.transform.position;
			GetComponent<Rigidbody> ().AddForce (fallingForce.normalized * 1000, ForceMode.Impulse);
		}

		state = State.ko;

		skinMaterial.color = Color.white;
	}

	public void ParryAttack(Character attacker) {
		skinMaterial.color = Color.white;
		RotateTowards (attacker.transform.position);
	}

	public void RotateTowards(Vector3 dest) {
		isRotating = true;
		timeStartedLerping = Time.time;
		lookRotation = Quaternion.LookRotation ((dest - transform.position).normalized);
		//transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation ((attacker.transform.position - transform.position).normalized), 30 * Time.deltaTime);
	}

	public void SetCurrentCC(CloseContact contact) {
		currentCC = contact;
	}

	public CloseContact GetCurrentCC() {
		return currentCC;
	}
}
