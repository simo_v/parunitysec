﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterUI : MonoBehaviour {

	public GameObject orderList;

	PlayableCharacter playable;

	PlayerController controller;

	// Use this for initialization
	void Start () { 
			
		orderList.SetActive (false);

		playable = GetComponentInParent<PlayableCharacter> ();

		controller = GameObject.Find ("PlayerController").GetComponent<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (controller.GetIsFrozen() && playable.GetIsSelected()) {
			orderList.SetActive (true);
		} else
			orderList.SetActive (false);
	}
}
