﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	//temp
	private int stunAmount = 4;

	public enum Type {melee,ranged}
	public Type weaponType;

	//statistiche arma
	[Header("Requisiti")]
	public int endurance;
	public int strenght;
	public int meleeMastery;
	public int fireArmMastery;
	[Space(0)]

	[Header ("Statistiche")]

	[Range(0,100)]
	public int rangedDamage = 10;

	[Range(0,100)]
	public int meleeDamage = 10;

	[Range(0,100)]
	public int meleeDefense = 10;

	[Range(0,100)]
	public float range = 300;

	[Range(0,100)]
	public float rangedPrecision = 300;

	[Range(0,100)]
	public float meleePrecision = 300;

	[Range(0,10000)]
	public float msRateOfFire = 100;

	[Range(0,10000)]
	public float msRateOfAttacks = 100;

	[Range(0.7f,4.0f)]
	public float aimTime = 2;

	[Range(0,100)]
	public int rangedEnergyDrain = 1;

	[Range(0,100)]
	public int meleeEnergyDrain = 1;

	[Range(1,10)]
	public float executionTime = 4;

	[Range(0,100)]
	public int executionEfficency = 50;

	[Space(20)]

	public projectileType type;

	private bool firstStrike = true;

	public Transform bulletSpawn;
	public int numberOfBulletsShot = 1;

	private float nextShotTime;

	public float projectileSpeed = 1000;
	public float projectileLife = 1;
	public enum projectileType {bullet, arrow, lazer}
	public Projectile newProjectile;

	private Character character;

	Projectile[] projectiles;

	void Start () {
		projectiles = new Projectile[numberOfBulletsShot];
		character = GetComponentInParent<Character> ();
	}
	
	void Update () {

		//resetta first strike se non sta attaccando
		if (!character.GetState ().Equals ("closeContact")) {
			firstStrike = true;
		}
	}

	public void Attack() {
		//attacca solo se l'energia non è a 0
		if (character.GetEnergy () > 0 || character.GetTargetObject().GetComponent<Character>().GetCurrentCC() != character.GetCurrentCC()) {
			if (Time.time > nextShotTime) {
				float rand = Random.Range (0, 0.1f);
				nextShotTime = Time.time + msRateOfAttacks / 1000 + rand;

				//se il bersaglio è un character ed è "ko" eseguilo
				if (character.GetTargetObject ().GetComponent<Character> () != null && character.GetTargetObject ().GetComponent<Character> ().GetState ().Equals ("ko")) {
					ExecuteTarget (character.GetTargetObject ());
				}
				//danneggialo se è un character
				else if (character.GetTargetObject().GetComponent<Character>() != null) {
					if (firstStrike) {
						character.SetEnergy (character.GetEnergy () - meleeEnergyDrain);
					}
					//danneggia l'avversario
					DamageTarget (character.GetTargetObject(), "melee");
				}
			}
		}
	}

	public void Shoot(){

		//spara solo se l'energia non è a 0
		if (character.GetEnergy () > 0) {
			
			//spara solo se è il momento
			if (Time.time > nextShotTime) {
				//se ho sparato setto il prossimo momento per sparare al tempo attuale + msBetweenShots
				float rand = Random.Range(0, 0.1f);
				nextShotTime = Time.time + msRateOfFire / 1000 + rand;

				//se spara più di un proiettile, spara ogni proiettile con una direzione leggermente differente
				if (numberOfBulletsShot > 1) {
					for (int i = 0; i < projectiles.Length; i++) {

						//crea un angolo random
						Vector3 randomAngle = transform.eulerAngles;
						randomAngle.y = Random.Range (-20.0f, 20.0f);
						Quaternion bulletAngle = Quaternion.Euler (bulletSpawn.rotation.eulerAngles.x, bulletSpawn.rotation.eulerAngles.y + randomAngle.y, bulletSpawn.rotation.eulerAngles.z);
					
						//crea un proiettile alla posizione bulletSpawn e rotazione che abbiamo trovato prima
						projectiles [i] = Instantiate (newProjectile, bulletSpawn.position, bulletAngle) as Projectile;

						//setta le variabili del proiettile
						projectiles [i].SetSpeed (projectileSpeed);
						projectiles [i].SetLife (projectileLife);
						projectiles [i].SetType ((int)type);
					}
					//se il bersaglio è un character ed è "ko" eseguilo
					if (character.GetTargetObject ().GetComponent<Character> () != null && character.GetTargetObject ().GetComponent<Character> ().GetState ().Equals ("ko")) {
						ExecuteTarget (character.GetTargetObject ());
					}
					//altrimenti danneggialo
					else {
						//danneggia l'avversario
						DamageTarget (character.GetTargetObject (), "ranged");
						//drena energia del character
						character.SetEnergy (character.GetEnergy () - rangedEnergyDrain);
					}
				} 

				//altrimenti spara il proiettile dritto
				else if (numberOfBulletsShot == 1) {

					//crea un proiettile alla posizione e rotazione bulletSpawn
					projectiles [0] = Instantiate (newProjectile, bulletSpawn.position, bulletSpawn.rotation) as Projectile;

					//setta le variabili del proiettile
					projectiles [0].SetSpeed (projectileSpeed);
					projectiles [0].SetLife (projectileLife);
					projectiles [0].SetType ((int)type);

					//se il bersaglio è un character ed è "ko" eseguilo
					if (character.GetTargetObject ().GetComponent<Character> () != null && character.GetTargetObject ().GetComponent<Character> ().GetState ().Equals ("ko")) {
						ExecuteTarget (character.GetTargetObject ());
					}
					//altrimenti danneggialo
					else if(character.GetTargetObject ().GetComponent<Character> () != null){
						//danneggia l'avversario
						DamageTarget (character.GetTargetObject (), "ranged");
						//drena energia del character
						character.SetEnergy (character.GetEnergy () - rangedEnergyDrain);
					}
				}
			}
		}
		//se invece è a 0
		else {
			character.SetCanShoot (false);
		}
	}


	void DamageTarget(GameObject targetObj, string attackType) {

		//RANGED
		if (attackType.Equals("ranged")) {

			float distance = Vector3.Distance (targetObj.transform.position, gameObject.transform.position);

			Character targetChar = targetObj.GetComponent<Character> ();

			if (distance < range) {
				if (targetChar) {
				
					if (!targetChar.GetState ().Equals ("ko") && targetChar.GetCanTakeDamage () == true) {

						int rand = 0;

						if (character.GetArea ().Equals ("none") || character.GetArea ().Equals ("green")) {
							rand = Random.Range (0, 100);
						} else if (character.GetArea ().Equals ("red")) {
							rand = 100;
						} else if (character.GetArea ().Equals ("yellow")) {
							if (targetChar.GetTargetObject () != null) {
								Vector3 dir = targetChar.GetTargetObject ().transform.position - targetChar.transform.position;
								if (targetChar.GetState ().Equals ("shooting") && Vector3.Angle (dir, targetChar.GetCover ().transform.forward) < 90)
									rand = Random.Range (30, 100);
								else
									rand = 100;
							}
						}
					
						//settare con parametri dell'arma e del character
						if (rand - rangedPrecision < 50) {
							//danneggia energia
							if (targetChar.GetEnergy () > 0) {
								targetChar.TakeEnergyDamage (rangedDamage, character);
							} 
							//danneggia punti vita
							else if (targetChar.GetEnergy () <= 0) {
								targetChar.TakeLifeDamage (character);
							}
						}
					}
				}
			} else {
				character.SetSkinColor (Color.red);
			}
		}

		//MELEE
		else if (attackType.Equals("melee")){

			Character targetChar = targetObj.GetComponent<Character> ();

			if (targetChar) {

				if (!targetChar.GetState ().Equals ("ko") || targetChar.GetCanTakeDamage () == true) {

					int rand = Random.Range (0, 100);

					//settare con parametri dell'arma e del character
					if (rand - meleePrecision < targetChar.GetDefenseShot()) {
						//danneggia energia
						if (targetChar.GetEnergy () > 0) {
							targetChar.TakeEnergyDamage (meleeDamage, character);
							targetChar.Stun (stunAmount); // lo stun può dipendere dai parametri di arma e character
							firstStrike = false;
						} 
						//danneggia punti vita
						else if (targetChar.GetEnergy () <= 0) {
							targetChar.TakeLifeDamage (character);
							firstStrike = false;
						}
					}
					// se l'attacco fallisce me l'ha parato, quindi passa lui all'attacco e io difendo
					else if(rand - meleePrecision > targetChar.GetDefenseShot()) {

						character.Stun (stunAmount); // lo stun può dipendere dai parametri di arma e character
						//isBeingAttacked = true solo se il character che è in turn sta puntando a me, altrimenti non sarò nè in turn nè attacked
						if (targetChar.GetTargetObject () == character.gameObject) {
							character.isBeingAttacked = true;
						} else {
							character.isBeingAttacked = false;
						}
						character.isInTurn = false;
						firstStrike = true;

						targetChar.isBeingAttacked = false;
						targetChar.isInTurn = true;
						targetChar.ParryAttack (character);
						targetChar.equippedWeapon.nextShotTime = Time.time + targetChar.equippedWeapon.msRateOfFire / 1000;
						targetChar.SetStun (false);
					}
				}
			}
		}
	}

	public float GetAimTime() {
		float rand = Random.Range (0f, 0.1f);
		return aimTime + rand;
	}

	void ExecuteTarget(GameObject targetObj) {
		Character targetChar = targetObj.GetComponent<Character> ();

		if (targetChar) {
			if (targetChar.GetDefenseShot () < executionEfficency) {
				targetChar.SetLife (0);
			} 

			else {
				character.SetEnergy (-1);
				character.SetSkinColor (Color.red);
				targetChar.SetEnergy (-1);

				targetChar.SetState ("idle");

				//sbalza indietro l'esecutore fallito 
				Vector3 fallingForce = character.transform.position - targetChar.transform.position;
				character.GetComponent<Rigidbody> ().AddForce (fallingForce.normalized * 3000, ForceMode.Impulse);

			}
		}
	}

	public string GetWeaponType() {
		return weaponType.ToString ();
	}
}
