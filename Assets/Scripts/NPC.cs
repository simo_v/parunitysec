﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

	public LayerMask mask;

	//private bool isWandering = true;
	private bool isResting = true;
	public bool isFleeing = false;
	private bool isShot = false;

	private float restingTime = 0;

	private List<Character> hearedCharacters = new List<Character>();

	private UnityEngine.AI.NavMeshAgent navMeshAgent;

	private Character character;
	//private PlayerController controller;
	public GameObject hearArea;

	public enum State{wander, flee}
	private State state;

	private Vector3 fleeFrom;
	private Vector3 randomPoint;
	//temp
	UnityEngine.AI.NavMeshHit newNavHit;

	// Use this for initialization
	void Start () {

		//controller = GameObject.FindObjectOfType<PlayerController> ();

		character = GetComponent<Character> ();

		navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent> ();

		state = State.wander;
	}
	
	// Update is called once per frame
	void Update () {

		//if i'm not fleeing
		if (isFleeing == false) {
			//and a character near me is shooting or is fighting
			foreach (Character item in hearedCharacters) {
				if(item.GetState().Equals("shooting")||item.GetState().Equals("closeContact")||item.GetState().Equals("executing")) {
					//flee and set the point to run away from
					FleeFrom (item.gameObject);
				}
			}
		}

		//if my energy is lower then maxEnergy
		if (character.GetEnergy () < character.maxEnergy && state != State.flee) {
			//it means i'm being shot, run away from where i am now
			FleeFrom (gameObject);
			isShot = true;
		}

		// STATES ----------------------------------------------------------------------------------------------------

		if (state == State.wander) {

			//while wandering the NPC will walk
			navMeshAgent.speed = 2;

			//if i'm not moving
			if (character.GetState().Equals("idle")) {
				//and if i'm not resting
				if (!isResting) {
					//set resting time to a random value, so that the NPC will take a random break between each walk
					restingTime = Random.Range (0, 10);
					isResting = true;
				}

				//decrease resting time
				restingTime -= Time.deltaTime;

				if (restingTime <= 0) {
					//not resting anymore
					isResting = false;

					//select a random point on the nav mesh
					randomPoint = new Vector3 (Random.Range (-240, 240), 0, Random.Range (-240, 240));

					UnityEngine.AI.NavMeshHit navHit;

					if (UnityEngine.AI.NavMesh.SamplePosition (randomPoint, out navHit, 4.0f, UnityEngine.AI.NavMesh.AllAreas)) {
						newNavHit = navHit;
						Ray ray = new Ray (navHit.position, Vector3.down * 10);

						RaycastHit hit;
						Physics.Raycast (ray, out hit, 100, mask);

						//give the NPC the order to move to that point
						character.GiveOrder ("moving", hit);
					}
				}
			}

			Debug.DrawRay (newNavHit.position, Vector3.down * 10, Color.yellow);

		}

		if (state == State.flee) {

			//while fleeing the NPC will run
			navMeshAgent.speed = 5;


			if (isFleeing == false) {

				isFleeing = true;

				//if i'm being shot or i'm in closeContact
				if (character.GetState ().Equals ("closeContact") || isShot) {

					isShot = false;

					//select the point to reach at random in the world and ensure it is distant enough from fleeFrom 
					do {
						randomPoint = new Vector3 (Random.Range (-240, 240), 0, Random.Range (-240, 240));
					
					} while((fleeFrom - randomPoint).magnitude < 50f);
				} 

				//else select a random point on a line that connects the NPC and the character shooting and ensure it is distant enough (double the distance from the two characters)
				else {
					Vector3 distance = fleeFrom - transform.position;

					randomPoint = transform.position + (Random.Range (distance.magnitude * 2, distance.magnitude * 5) * distance.normalized);
				}

				UnityEngine.AI.NavMeshHit navHit;

				if (UnityEngine.AI.NavMesh.SamplePosition (randomPoint, out navHit, 10.0f, UnityEngine.AI.NavMesh.AllAreas)) {
					newNavHit = navHit;
					Ray ray = new Ray (navHit.position, Vector3.down * 10);

					RaycastHit hit;
					Physics.Raycast (ray, out hit, 100, mask);

					//give the NPC the order to move
					character.GiveOrder ("moving", hit);
				}
			}


			//if the NPC is fleeing but it is in idle, it means it reached the destination, make it wander
			if (character.GetState ().Equals ("idle") && isFleeing) {
				isFleeing = false;
				state = State.wander;
			}

		}
	}

	public void AddHearedCharacter(Character value) {
		hearedCharacters.Add (value);
	}

	public void RemoveHearedCharacter(Character value) {
		hearedCharacters.Remove (value);
	}

	void FleeFrom(GameObject target) {
		fleeFrom = target.transform.position;
		state = State.flee;
	}
}
