﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public bool alarmed = false;

	public LayerMask mask;

	public GameObject battleSphere;
	public GameObject viewCone;

	private bool isResting = true;

	private float restingTime = 0;

	private Vector3 randomPoint;

	public enum State {wander, stay, battle}
	private State state;

	private State initialState;

	private Character character;

	private UnityEngine.AI.NavMeshAgent navMeshAgent;

	private List<Character> hearedCharacters = new List<Character>();
	private List<PlayableCharacter> playersInBattle = new List<PlayableCharacter>();
	private List<Enemy> enemiesInBattle = new List<Enemy>();

	private Character targetedPlayer;

	// Use this for initialization
	void Awake () {
		character = GetComponentInParent<Character> ();

		navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent> ();

		state = initialState;

		battleSphere.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {

		// STATES ----------------------------------------------------------------------------------------------------

		if (state == State.wander) {

			battleSphere.SetActive (false);

			//while wandering the Enemy will walk
			navMeshAgent.speed = 2;

			//if i'm not moving
			if (character.GetState().Equals("idle")) {
				//and if i'm not resting
				if (!isResting) {
					//set resting time to a random value, so that the NPC will take a random break between each walk
					restingTime = Random.Range (0, 10);
					isResting = true;
				}

				//decrease resting time
				restingTime -= Time.deltaTime;

				if (restingTime <= 0) {
					//not resting anymore
					isResting = false;

					//select a random point on the nav mesh
					randomPoint = new Vector3 (Random.Range (-240, 240), 0, Random.Range (-240, 240));

					UnityEngine.AI.NavMeshHit navHit;

					if (UnityEngine.AI.NavMesh.SamplePosition (randomPoint, out navHit, 4.0f, UnityEngine.AI.NavMesh.AllAreas)) {
						Ray ray = new Ray (navHit.position, Vector3.down * 10);

						RaycastHit hit;
						Physics.Raycast (ray, out hit, 100, mask);

						//give the NPC the order to move to that point
						character.GiveOrder ("moving", hit);
					}
				}
			}

			//se uno dei personaggi in hearedCharacters spara o fa qualcosa che attira l'attenzione
				//ferma e gira l'enemy verso il rumore, il viewCone farà il resto

			if (alarmed) {
				state = State.battle;
			}
		}

		if (state == State.battle) {

			//while in battle the Enemy will run
			navMeshAgent.speed = 6;

			battleSphere.SetActive (true);

			//aggiungi tutti i players e gli enemy nelle rispettive list

			if (targetedPlayer) {
				//se arma melee attacking

				//se arma ranged shooting
			}
		}

		if (state == State.stay) {
			battleSphere.SetActive (false);
		}
	}

	public void Alarm(Character target) {
		if (state != State.battle) {
			alarmed = true;
			targetedPlayer = target;
		}
	}

	public void AddHearedCharacter(Character value) {
		hearedCharacters.Add (value);
	}

	public void RemoveHearedCharacter(Character value) {
		hearedCharacters.Remove (value);
	}

}
