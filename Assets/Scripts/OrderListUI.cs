﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderListUI : MonoBehaviour {

	private Character character;
	private PlayableCharacter playable;


	public GameObject[] ordersText;

	public UnityEngine.UI.Text life;

	public UnityEngine.UI.Text characterName;

	private Camera cam;

	void Start () {

		character = GetComponentInParent<Character> ();
		playable = GetComponentInParent<PlayableCharacter> ();


		foreach (GameObject item in ordersText) {
			item.SetActive (false);
		}

		cam = Camera.main;

		//rivolto sempre verso la camera
		transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);
	}
	
	void Update () {


		//rivolto sempre verso la camera
		transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);

		// per ogni ordine assegnato al character scrivilo sulla order list
		for (int i = 0; i < character.GetOrders ().Count; i++) {
			ordersText [i].SetActive (true);
			UnityEngine.UI.Text order = ordersText [i].GetComponent<UnityEngine.UI.Text> ();
			order.text = character.GetOrders () [i].name;
		}

		//le altre righe della order list vengono cancellate
		for (int i = character.GetOrders ().Count; i < 8; i++) {
			ordersText [i].SetActive (false);
		}

		//riporto la vita del personaggio
		life.text = character.GetLife().ToString();

		characterName.text = playable.characterName;
	}
}
