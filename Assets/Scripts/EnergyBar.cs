﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBar : MonoBehaviour {

	public GameObject bar;
	public UnityEngine.UI.Image energyBar;
	public UnityEngine.UI.Image restBar;
	public UnityEngine.UI.Text life;


	private float timer = 5;
	private float resultEnergy;
	private float resultRest;
	private float unit;

	private Camera cam;

	private Character character;

	// Use this for initialization
	void Start () {
		character = GetComponentInParent<Character> ();
		unit = character.GetMaxEnergy () / 100;

		cam = Camera.main;

		//rivolto sempre verso la camera
		transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);
	}
	
	// Update is called once per frame
	void Update () {

		life.text = character.GetLife ().ToString ();

		//rivolto sempre verso la camera
		transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);

		//divido l'energia del character per l'unità trovata, e poi di nuovo diviso 100 per trovare quanto riempire la barra dell'energia
		resultEnergy = character.GetEnergy () / unit / 100;
		energyBar.fillAmount = resultEnergy;

		//faccio stessa cosa per il riposo
		resultRest = character.GetRest()/unit/100;
		restBar.fillAmount = resultRest;

		//se per 5 secondi l'energia è al massimo non mostro la barra, non serve. Altrimenti la mostro
		if (character.GetEnergy () == character.GetMaxEnergy ()) {
			timer -= Time.deltaTime;
			if (timer < 0) {
				bar.SetActive (false);
				life.gameObject.SetActive(false);
			}
		} else {
			timer = 5;
			bar.SetActive(true);
			life.gameObject.SetActive(true);

		}
	}
}
