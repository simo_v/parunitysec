﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursors : MonoBehaviour {

	public enum Type {none, attack, cover};
	public Type type;

	private List<Character> selectedChars = new List<Character> ();

	public GameObject attackCursor;
	public GameObject coverCursor;
	public GameObject outOfRangeCursor;

	LineRenderer line;

	private PlayerController controller;

	Camera cam;

	RaycastHit hit;

	public LayerMask mask;

	// Use this for initialization
	void Start () {
		
		controller = GameObject.FindObjectOfType<PlayerController> ();

		cam = Camera.main;

		line = gameObject.GetComponent<LineRenderer>();
		line.enabled = false;
		line.startWidth = 0f; 
		line.endWidth = 0.5f;
		line.startColor = Color.yellow;
		line.endColor = Color.red;
	
	}
	
	// Update is called once per frame
	void Update () {
		
		foreach (PlayableCharacter item in controller.GetPlayableCharacters()) {
			if (item.GetIsSelected () == true) {
				selectedChars.Add (item.gameObject.GetComponent<Character>());
			}
			else if(selectedChars.Contains(item.gameObject.GetComponent<Character>())) {
				selectedChars.Remove(item.gameObject.GetComponent<Character>());
			}
		}
	

		if (type == Type.attack) {
			
			//rivolto sempre verso la camera
			transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);

			foreach (Character item in selectedChars) {
				float distance = Vector3.Distance (item.transform.position, hit.collider.transform.position);
				if (distance > item.equippedWeapon.range) {
					outOfRangeCursor.SetActive (true);
					attackCursor.SetActive (false);
					//Cursor.visible = false;
					transform.position = hit.collider.transform.position;
					//boh
					Vector3 rangeLimit = hit.collider.transform.position;
					//A = A + BC/distanza * range; BC = C - B;
					rangeLimit += (item.transform.position - hit.collider.transform.position)/distance * item.equippedWeapon.range;

					line.enabled = true;
					Vector3[] positions = { hit.collider.transform.position, rangeLimit };
					line.SetPositions (positions);
					} 

				else {
					line.enabled = false;

					outOfRangeCursor.SetActive (false);
					attackCursor.SetActive (true);
					//Cursor.visible = false;
					transform.position = hit.collider.transform.position;

				}
			}
			if (attackCursor) {
				attackCursor.transform.Rotate (Vector3.forward * 200 * Time.deltaTime);
			}

		}

		if (type == Type.none) {
			attackCursor.SetActive (false);
			coverCursor.SetActive (false);
			outOfRangeCursor.SetActive (false);

			line.enabled = false;


			Cursor.visible = true;
		}

		if (type == Type.cover) {
			coverCursor.SetActive (true);
			Cursor.visible = false;
			line.enabled = false;

			transform.position = hit.collider.transform.position;
			transform.rotation = hit.collider.transform.rotation ;
		}




		Ray ray = cam.ScreenPointToRay (Input.mousePosition);
		Plane groundPlane = new Plane (Vector3.up, Vector3.zero);
			
		Physics.Raycast (ray, out hit, 1000, mask);
		if (selectedChars.Count > 0) {
			if (hit.collider.tag == "NPC") {

				type = Type.attack;	
			} else if (hit.collider.tag == "Cover" && hit.collider.gameObject.GetComponent<Cover> ().isBeingUsed == false) {

				type = Type.cover;
			} else
				type = Type.none;
		} else
			type = Type.none;
	}
}
