﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseContact : MonoBehaviour {

	private bool destroyed = false;

	private List<Character> charactersInCC = new List<Character>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		//se è rimasto solo un character nel close contact oppure nessuno è attaccato
		if (charactersInCC.Count <= 1 || NooneIsAttacked()) {
			foreach (Character item in charactersInCC) {
				//ridò l'ordine di attaccare a quelli che vogliono farlo (wantsToAttack=true) NON FUNZIONA BENE
				/*if (item.wantsToAttack = true) {
					Ray ray = new Ray (Vector3.up, item.GetTargetObject ().transform.position);
					RaycastHit hit;
					Physics.Raycast (ray, out hit);
					item.GiveOrder("attacking", hit);
				}*/
				//distruggo il CC e lo elimino dalle referenze dei character
				item.SetCurrentCC (null);
				if (item.GetState ().Equals ("closeContact")) {
					item.SetState ("idle");
				}
			}
			Destroy (gameObject);
			destroyed = true;
		}

		//se c'è almeno un character con energy > 0 restituisco false
		bool check = true;
		foreach (Character item in charactersInCC) {
			if (item.GetEnergy() > 0) {
				check = false;
				break;
			}
		}

		//se invece è true distruggo il close contact e lo libero dai character
		if(check==true) {
			foreach (Character item in charactersInCC) {
				item.SetCurrentCC (null);
				if (item.GetState ().Equals ("closeContact")) {
					item.SetState ("idle");
				}
			}
			Destroy (gameObject);
			destroyed = true;
		}

		//se nessuno sta attaccando (significa che quello che stava attaccando se ne è andato ma ci sono altri personaggi nel CC) 
		//scegline uno che puntava a quello attaccato e dagli il turno
		if (NooneIsInTurn () && !destroyed) {
			foreach (Character item in charactersInCC) {
				if (item.GetTargetObject () != null) {
					if (item.GetTargetObject () == GetIsBeingAttacked ().gameObject && item.GetIsStunned () == false) {
						item.isInTurn = true;
					}
				}
			}
		}
	}

	public void Insert(Character character) {
		charactersInCC.Add (character);
		character.SetState ("closeContact");
		character.SetCurrentCC (this);

		/*for(int i = 0; i < charactersInCC.Count;i++) {
			Debug.Log (i +" "+ charactersInCC[i]+" "+ charactersInCC[i].GetTargetObject());
		}*/
	}

	/*public Character selectRandomEnemy(Character attacker) {
		
	}*/

	public void RemoveCharacter(Character character) {
		charactersInCC.Remove (character);
		character.SetCurrentCC (null);

		/*for(int i = 0; i < charactersInCC.Count;i++) {
			Debug.Log (i +" "+ charactersInCC[i]+" "+charactersInCC[i].GetTargetObject());
		}*/
	}

	public Character GetAttacker() {
		for (int i = 0; i < charactersInCC.Count; i++) {
			if (charactersInCC [i].isInTurn) {
				return charactersInCC[i];
			}
		}
		return null;
	}

	public List<Character> GetCharactersInCC() {
		return charactersInCC;
	}


	public void SkipTurn(Character value) {
		foreach (Character item in GetAttackersOfCharacter(value)) {
			//se c'è un nemico che non è stunnato
			if (item.GetIsStunned () == false) {
				value.isInTurn = false;
				value.isBeingAttacked = true;

				item.isInTurn = true;
				item.isBeingAttacked = false;
			}
		}
	}

	public List<Character> GetAttackersOfCharacter(Character value) {

		List<Character> attackers = new List<Character> ();

		//ciclo fra tutti i character nel CC
		foreach (Character item in charactersInCC) {

			//inserisco in una nuova List tutti quelli che attaccano il character selezionato
			if (item.GetTargetObject() == value.gameObject) {
				attackers.Add (item);
			}
		}

		return attackers;
	}

	public bool AllAttackersInStun(Character value) {

		//vedo se sono tutti gli attackers sono in stun, se qualcuno non lo è restituisco false, altrimenti true
		foreach (Character item in GetAttackersOfCharacter(value)) {
			if (item.GetIsStunned () == false) {
				return false;
			}
		}
		return true;
	}

	public bool NooneIsStealingTurn(Character value) {

		//vedo se nessuno degli attackers isStealingTurn = true
		foreach (Character item in GetAttackersOfCharacter(value)) {
			if (item.isStealingTurn == true) {
				return false;
			}
		}
		return true;
	}

	public bool NooneIsAttacked() {
		foreach (Character item in charactersInCC) {
			if (item.isBeingAttacked == true) {
				return false;
			}
		}
		return true;
	}

	public bool NooneIsInTurn() {
		foreach (Character item in charactersInCC) {
			if (item.isInTurn == true) {
				return false;
			}
		}
		return true;
	}

	public Character GetIsBeingAttacked() {
		for (int i = 0; i < charactersInCC.Count; i++) {
			if (charactersInCC [i].isBeingAttacked) {
				return charactersInCC[i];
			}
		}
		return null;
	}
}
