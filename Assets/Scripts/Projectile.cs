﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	private float speed;
	private float life;
	private enum Type {bullet, arrow, lazer};
	private Type type;

	//TODO array of meshes
	public GameObject meshBullet;
	public GameObject meshArrow;
	public GameObject meshLazer;


	void Start() {
		if (type == Type.bullet) {
			meshArrow.SetActive (false);
			meshLazer.SetActive (false);
		} 
		else if (type == Type.arrow) {
			meshBullet.SetActive (false);
			meshLazer.SetActive (false);
		}
		else if (type == Type.lazer) {
			meshBullet.SetActive (false);
			meshArrow.SetActive (false);

		}

	}

	void Update () {
		//move bullet forward
		transform.Translate (Vector3.forward * Time.deltaTime * speed /** Time.timeScale*/);

		//destroy bullet
		life -= Time.deltaTime;
		if (life < 0)
			Destroy (gameObject);
	}

	public void SetSpeed(float bulletSpeed){
		speed = bulletSpeed;
	}

	public void SetLife(float bulletLife){
		life = bulletLife;
	}

	public void SetType(int bulletType){
		if (bulletType == 0)
			type = Type.bullet;
		if (bulletType == 1)
			type = Type.arrow;
		if (bulletType == 2)
			type = Type.lazer;
	}
}
